;;;; Fibonacci sequence with recursive & tail-recursive implementation.

;;; Calculates nth element of the Fibonacci sequence recursively.

(defun fib (n) 
	(if (<= n 1)
		1
		(+ (fib (- n 1)) (fib (- n 2)))
	)
)

;;; Calculates nth element of the Fibonacci sequence with a tail-recursive algorithm.

(defun fib2 (n)
	(fib_tail n 1 0)
)

;;; Tail-recursive algorithm that calculates nth element of the Fibonacci sequence.

(defun fib_tail (n curr prev) 
	(if (= n 0)
		curr
		(fib_tail (- n 1) (+ curr prev) curr)
	)
)
